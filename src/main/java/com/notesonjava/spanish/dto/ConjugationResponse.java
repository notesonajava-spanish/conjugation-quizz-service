package com.notesonjava.spanish.dto;

import java.util.ArrayList;
import java.util.List;

import com.notesonjava.spanish.model.Conjugation;

import lombok.Data;

@Data
public class ConjugationResponse {

	private List<Conjugation> conjugations = new ArrayList<>();
	private ConjugationQuery query;

}