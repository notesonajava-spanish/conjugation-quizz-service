package com.notesonjava.spanish.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class ConjugationQuery {

	private List<String> verbs = new ArrayList<>();
	private List<String> tiempos = new ArrayList<>(); 
	
	
}