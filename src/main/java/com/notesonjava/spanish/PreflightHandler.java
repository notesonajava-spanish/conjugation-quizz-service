package com.notesonjava.spanish;

import io.javalin.Context;
import io.javalin.Handler;

public class PreflightHandler implements Handler {
	
	private static final String EMPTY_BODY = "";
	
	@Override
	public void handle(Context ctx) throws Exception {
		ctx.status(200);
		ctx.result(EMPTY_BODY);
	}
}