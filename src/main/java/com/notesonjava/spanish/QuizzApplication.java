package com.notesonjava.spanish;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.TimeoutException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.notesonjava.auth.QuizzAccessManager;
import com.notesonjava.auth.UserHandler;
import com.notesonjava.config.PropertyLoader;
import com.notesonjava.config.PropertyMap;
import com.notesonjava.spanish.clients.ConjugationReportClient;
import com.notesonjava.spanish.clients.ConjugationReportUrlBuilder;
import com.notesonjava.spanish.clients.VerbClient;
import com.notesonjava.spanish.clients.VerbUrlBuilder;
import com.notesonjava.spanish.conjugation.ConjugationClient;
import com.notesonjava.spanish.conjugation.ConjugationQuizzController;
import com.notesonjava.spanish.conjugation.ConjugationQuizzService;
import com.notesonjava.spanish.conjugation.ConjugationUrlBuilder;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import io.javalin.Javalin;
import io.javalin.json.JavalinJackson;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class QuizzApplication {
	
	public static final String CONJUGATION_QUEUE = "AnswersConjugations";
	
	private static Channel openChannel(Connection connection) {
		Channel channel = null;
    	
    	while(channel == null) {
		    try {
	    		channel = connection.createChannel();
		    } catch (Exception e) {
	    		log.warn(e.getMessage());
	    		try {
	    			log.warn("Wait 1 second before retrying channel");
	    			Thread.sleep(1000);
	    		} catch(Exception e2) {
	    			log.warn(e2.getMessage());
	    		}
	    	}
	    }
		return channel;
	}

	private static Connection openConnection(ConnectionFactory factory) {
		Connection connection = null;
    	while(connection == null) {
		    try {
	    		connection = factory.newConnection();		    	
		    } catch (Exception e) {
	    		log.warn(e.getMessage());
	    		try {
	    			log.warn("Wait 1 second before retrying connection");
	    			Thread.sleep(1000);
	    		} catch(Exception e2) {
	    			log.warn(e2.getMessage());
	    		}
	    	}
	    }
		return connection;
	}

	
	
	public static void main(String[] args) throws IOException, TimeoutException {
		PropertyMap prop = PropertyLoader.loadProperties();
		String portValue = prop.get("server.port").orElse("8080");
		String verbServiceUrl = prop.get("verb.service.url").orElse("http://localhost:8081");
		String conjugationServiceUrl = prop.get("conjugation.service.url").orElse("http://localhost:8082");
		String conjugationReportUrl = prop.get("conjugation.report.url").orElse("http://localhost:8083");
		
		String authUserUrl = prop.get("auth.user.url").orElse("https://fmottard.auth0.com/userinfo");
	
		String mqHost = prop.get("mq.host").orElse("localhost");
	
    	log.info("Create the queues");
    	ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(mqHost);
    	    	
        log.info("Create the channel");
    	
        Connection connection = openConnection(factory);
	    Channel channel = openChannel(connection);
	    channel.queueDeclare(CONJUGATION_QUEUE, false, false, false, null);

		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		JavalinJackson.configure(mapper);
		
		PreflightHandler preflightHandler = new PreflightHandler();
		
		VerbUrlBuilder verbUrlBuilder = new VerbUrlBuilder(verbServiceUrl);
		VerbClient verbClient = new VerbClient(verbUrlBuilder, mapper);
		UserHandler userHandler = new UserHandler();
		
		ConjugationUrlBuilder conjugationUrlBuilder = new ConjugationUrlBuilder(conjugationServiceUrl);
		ConjugationClient conjugationClient = new ConjugationClient(conjugationUrlBuilder, mapper);
		ConjugationReportUrlBuilder reportUrlBuilder = new ConjugationReportUrlBuilder(conjugationReportUrl);
		ConjugationReportClient reportClient = new ConjugationReportClient(reportUrlBuilder, mapper);
		
		ConjugationQuizzService conjugationService = new ConjugationQuizzService(conjugationClient, verbClient, reportClient);
		ConjugationQuizzController conjugationController = new ConjugationQuizzController(conjugationService, mapper, userHandler, channel, CONJUGATION_QUEUE, preflightHandler);
		
		Javalin app = Javalin.create()
				.enableCorsForAllOrigins()
				.port(Integer.parseInt(portValue));
		
		app.defaultContentType("application/json;charset=utf-8");
		app.requestLogger((ctx, timeMs) -> {
		    System.out.println(ctx.method() + " "  + ctx.path() + " took " + timeMs + " ms");
		    // prints "GET /hello took 4.5 ms"
		});
		
		app.get("/ping", ctx -> {
			Optional<String> user = userHandler.retrieveUser(ctx);
			if(user.isPresent()) {
				log.info("User: " + user.get());
			} else {
				log.info("Missing user");
			}
			ctx.result("I am alive : " +user.orElse("Missing User"));
		});
		
		QuizzAccessManager accessManager = new QuizzAccessManager(authUserUrl, mapper);
		
		app.accessManager((handler, ctx, permittedRoles) -> accessManager.manage(handler, ctx, permittedRoles));	
		conjugationController.init(app);
		app.start();
		
		Runtime.getRuntime().addShutdownHook(new Thread() {
	    	public void run() {
	    		closeChannel(channel); 
	    		closeConnection(connection); 
	    	}

			private void closeConnection(Connection connection) {
				try {
	    			if(connection != null) {
	    				connection.close();
	    			}
				} catch (Exception e) {
					log.error("Unable to close connection", e);
				}
			}

			private void closeChannel(Channel channel) {
				try {
	    			if(channel != null) {
	    				channel.close();
	    			}	
				} catch (Exception e) {
					log.error("Unable to close connection", e);
				}
			}
	    });
	}
}

