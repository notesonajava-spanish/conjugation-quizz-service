package com.notesonjava.spanish.model;

public enum Persona {

	//NONE(0), YO(1), TU(2), EL(5), NOSOTROS(8), ELLOS(13);
	
	
	NONE(0),
	YO(1),
	TU(2),
	VOS(4),
	EL(5),
	ELLO(7),
	NOSOTROS(8),
	VOSOTROS(10),
	ELLOS(13);

	private int value;

	private Persona(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public static Persona valueOf(int value) {
		for (Persona p : Persona.values()) {
			if (p.getValue() == value) {
				return p;
			}
		}
		return Persona.NONE;
	}
}
