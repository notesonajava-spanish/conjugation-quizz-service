package com.notesonjava.spanish.model;

import java.time.LocalDate;
import java.time.ZonedDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
public abstract class AbstractAnswer {
	
	private String username;
	
	private String answer;
	
	private ZonedDateTime answerTime;
	
	private int score;
	
	@JsonIgnore
	public LocalDate getAnswerDay(){
		return answerTime.toLocalDate();
	}
}