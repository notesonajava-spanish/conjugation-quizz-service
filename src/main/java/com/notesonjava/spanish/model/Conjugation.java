package com.notesonjava.spanish.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Conjugation{
	
	private String word;
	
	private Tiempo tiempo;
	
	private Persona persona;
	
	private String verb;
	

	
}

