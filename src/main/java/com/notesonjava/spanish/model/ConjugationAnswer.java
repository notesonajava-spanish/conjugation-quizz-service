package com.notesonjava.spanish.model;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString(callSuper=true)
@EqualsAndHashCode(callSuper=true)
public class ConjugationAnswer extends AbstractAnswer{
	
	private Conjugation conjugation;
	
	public ConjugationAnswer(Conjugation c, String username){
		this.conjugation = c;
		this.setUsername(username);
		this.setAnswerTime(ZonedDateTime.now(ZoneOffset.UTC));
	}
	
}
