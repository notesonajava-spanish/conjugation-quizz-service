package com.notesonjava.spanish.conjugation;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.notesonjava.spanish.model.Tiempo;
import com.notesonjava.spanish.model.VerbConjugation;

import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@AllArgsConstructor
@Slf4j
public class ConjugationClient {

	
	private ConjugationUrlBuilder builder;

	private ObjectMapper mapper;
	
	public List<VerbConjugation> getConjugations(List<String> verbs, Tiempo tiempo) throws JsonParseException, JsonMappingException, IOException{
		String url = builder.conjugation(verbs,  tiempo);
		log.info("Call : " + url);
		
		HttpResponse response = HttpRequest.get(url).send();;
		VerbConjugation[] conjugations = mapper.readValue(response.bodyBytes(), VerbConjugation[].class);
		return Arrays.asList(conjugations);
	}
	
	  
	    

	    public List<VerbConjugation> search(List<String> verbs, List<Tiempo> tiempos) throws JsonParseException, JsonMappingException, IOException {
	    	

	        String url = builder.search(verbs, tiempos);
	        log.info("Call : " + url);
	        HttpResponse response = HttpRequest.get(url).send();;
	        VerbConjugation[] conjugations = mapper.readValue(response.bodyBytes(), VerbConjugation[].class);
			return Arrays.asList(conjugations);
	    }

	    public List<VerbConjugation> findByVerb(String verb) throws JsonParseException, JsonMappingException, IOException {
	        String url = builder.findByVerb(verb);
	        log.info("Call : " + url);
	        HttpResponse response = HttpRequest.get(url).send();;
	        VerbConjugation[] conjugations = mapper.readValue(response.bodyBytes(), VerbConjugation[].class);
			return Arrays.asList(conjugations);
	    }

	    public List<VerbConjugation> findByTiempo(Tiempo tiempo) throws JsonParseException, JsonMappingException, IOException {
	        String url = builder.findByTiempo(tiempo);
	        HttpResponse response = HttpRequest.get(url).send();;
	        VerbConjugation[] conjugations = mapper.readValue(response.bodyBytes(), VerbConjugation[].class);
			return Arrays.asList(conjugations);
	    }

	    public List<VerbConjugation> findByVerbAndTiempo(String verb, Tiempo tiempo) throws JsonParseException, JsonMappingException, IOException {
	        String url = builder.findByVerbAndTiempo(verb, tiempo);
	        log.info("Call : " + url);
	        HttpResponse response = HttpRequest.get(url).send();;
	        
	        VerbConjugation[] conjugations = mapper.readValue(response.bodyBytes(), VerbConjugation[].class);
			return Arrays.asList(conjugations);
	    }

	    public List<VerbConjugation> findByVerbSuffixAndTiempo(String suffix, Tiempo tiempo) throws JsonParseException, JsonMappingException, IOException {
	        String url = builder.findByVerbSuffixAndTiempo(suffix, tiempo);
	        log.info("Call : " + url);
	        HttpResponse response = HttpRequest.get(url).send();;
	        VerbConjugation[] conjugations = mapper.readValue(response.bodyBytes(), VerbConjugation[].class);
			return Arrays.asList(conjugations);
	    }

	    public List<VerbConjugation> findByVerbSuffixAndTiempos(String suffix, List<Tiempo> tiempos) throws JsonParseException, JsonMappingException, IOException {
	        String url = builder.findByVerbSuffixAndTiempos(suffix, tiempos);
	        log.info("Call : " + url);
	        HttpResponse response = HttpRequest.get(url).send();;
	        VerbConjugation[] conjugations = mapper.readValue(response.bodyBytes(), VerbConjugation[].class);
			return Arrays.asList(conjugations);
	    }


	    public List<VerbConjugation> findByTiempoAndVerbNotIn(Tiempo tiempo, List<String> verbs) throws JsonParseException, JsonMappingException, IOException {
	        String url = builder.findByTiempoAndVerbNotIn(tiempo, verbs);
	        log.info("Call : " + url);
	        HttpResponse response = HttpRequest.get(url).send();;
	        VerbConjugation[] conjugations = mapper.readValue(response.bodyBytes(), VerbConjugation[].class);
			return Arrays.asList(conjugations);
	    }

	
}