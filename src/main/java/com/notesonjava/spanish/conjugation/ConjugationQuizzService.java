package com.notesonjava.spanish.conjugation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.notesonjava.spanish.clients.ConjugationReportClient;
import com.notesonjava.spanish.clients.VerbClient;
import com.notesonjava.spanish.model.Conjugation;
import com.notesonjava.spanish.model.Persona;
import com.notesonjava.spanish.model.Tiempo;
import com.notesonjava.spanish.model.Verb;
import com.notesonjava.spanish.model.VerbConjugation;
import com.notesonjava.spanish.model.WordStats;

import io.reactivex.Flowable;
import io.reactivex.Single;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ConjugationQuizzService {
	
	private static final List<Tiempo> tiempos = Arrays.asList(Tiempo.IndicativoPresente, Tiempo.IndicativoPreteritoPerfectoSimple, Tiempo.IndicativoPreteritoimperfecto, Tiempo.ImperativoPresente, Tiempo.IndicativoFuturo, Tiempo.IndicativoCondicional, Tiempo.Participio, Tiempo.Gerundio, Tiempo.GerundioCompuesto, Tiempo.IndicativoFuturoPerfecto);
	
	private ConjugationClient client;

	private VerbClient verbClient;
	
	
	private ConjugationReportClient reportClient;
	
	public List<String> getImportantVerbs() {
		return Arrays.asList("estar", "ser", "tener", "poner", "poder", "hacer", "dar", "caer", "haber", "traer", "caber", "prestar", "llevar", "llover", "llorar");
	}
	
	public List<Conjugation> generateRandom(String username, int size) throws JsonParseException, JsonMappingException, IOException{
		Random rand = new Random();
		int index = rand.nextInt(tiempos.size());
		Tiempo tiempo = tiempos.get(index);
		
		List<Verb> verbs = verbClient.getPopularVerbs(500);
		Collections.shuffle(verbs);		
		List<String> query = Flowable.fromIterable(verbs)
					.map(v -> v.getSpanish())
					.toList().blockingGet();

		return generate(username, query, tiempo, size);
	}
	
	public List<Conjugation> generate(String username, List<Persona> personas, int size) throws JsonParseException, JsonMappingException, IOException {
	
		Flowable<String> verbFlow = Flowable.fromIterable(verbClient.getPopularVerbs(500)).map(v -> v.getSpanish());
		
		Single<WordStats<Tiempo>> tiempoFlow = Flowable.fromIterable(reportClient.getTiempoReport(username)).reduce( (s1, s2) -> (s1.getScore() > s2.getScore())? s2: s1).toSingle();
		List<String> verbs = verbFlow.toList().blockingGet();
		Tiempo selected = tiempoFlow.blockingGet().getItem();
		
		List<Conjugation> list = Flowable.fromIterable(client.getConjugations(verbs, selected))
				.map(vc -> convert(vc))
				.flatMap(vcList -> Flowable.fromIterable(vcList))
				.filter(c -> personas.contains(c.getPersona()))
				.toList().blockingGet();
			
		Collections.shuffle(list);
		if(list.size()>size){
			return list.subList(0, size);
		}
		return list;
	}
	
	public List<Conjugation> generate(String username, List<String> verbs, Tiempo tiempo, int size) throws JsonParseException, JsonMappingException, IOException {
		
		List<Conjugation> list = Flowable.fromIterable(client.getConjugations(verbs, tiempo))
				.map(vc -> convert(vc))
				.flatMap(vcList -> Flowable.fromIterable(vcList))
				.toList().blockingGet();
		
		Collections.shuffle(list);
		if(list.size() > size){
			return list.subList(0, size);
		}
		return list;
	}
	
	private List<Conjugation> convert(VerbConjugation vc) {
		List<Conjugation> conjugations = new ArrayList<>();
		vc.getConjugations().forEach(c -> {
			Conjugation conjugation = new Conjugation();
			conjugation.setVerb(vc.getVerb());
			conjugation.setTiempo(vc.getTiempo());
			conjugation.setWord(c.getWord());
			conjugation.setPersona(c.getPersona());
			
			conjugations.add(conjugation);
		});
		return conjugations;
	}
	
}