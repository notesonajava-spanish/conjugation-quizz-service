package com.notesonjava.spanish.conjugation;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.notesonjava.auth.UserHandler;
import com.notesonjava.spanish.PreflightHandler;
import com.notesonjava.spanish.dto.ConjugationAnswerDto;
import com.notesonjava.spanish.dto.ConjugationQuestion;
import com.notesonjava.spanish.model.Conjugation;
import com.notesonjava.spanish.model.ConjugationAnswer;
import com.rabbitmq.client.Channel;

import io.javalin.Javalin;
import jodd.http.HttpStatus;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class ConjugationQuizzController {

	private ConjugationQuizzService conjugationService;
	private ObjectMapper mapper;
	
	
	private UserHandler userHandler;
	
	private Channel channel;
	private String queue;
	
	
	private PreflightHandler preflightHandler;
	
	private static final String APPLICATION_JSON_UTF8 = "application/json;charset=utf-8";
	
	private static final String AUTH = "Authorization";
	
	private static final String BEARER = "Bearer";
	
	
	public void init(Javalin app) {
		app.get("/create", ctx ->{
			Optional<String> user = userHandler.retrieveUser(ctx);
			ctx.contentType(APPLICATION_JSON_UTF8);
			
			if(user.isPresent()) {
				String token = ctx.header(AUTH).substring(BEARER.length()).trim();
				
				List<Conjugation> conjugationList = conjugationService.generateRandom(token, 10);
				List<ConjugationQuestion> questions = new ArrayList<>();
				conjugationList.forEach(c -> {
					ConjugationQuestion question = new ConjugationQuestion();
					question.setConjugation(c);
					question.addAcceptedAnswer(c.getWord());
					question.addAcceptedAnswer(StringUtils.stripAccents(c.getWord())); 
					questions.add(question);
				});
				
				String json = mapper.writeValueAsString(questions);
				ctx.status(HttpStatus.HTTP_OK);
				ctx.result(json);
			}
		});
		
		app.options("/answers",  preflightHandler);
		
		
		app.post("/answers", ctx -> {
			Optional<String> user = userHandler.retrieveUser(ctx);
			if(user.isPresent()) {
				ConjugationAnswerDto[] answers =  mapper.readValue(ctx.body(), ConjugationAnswerDto[].class);
				for(ConjugationAnswerDto answer : answers){
					ConjugationAnswer a = new ConjugationAnswer(answer.getQuestion(), user.get());
					a.setAnswer(answer.getAnswer());
					a.setAnswerTime(answer.getAnswerTime());
					a.setScore(answer.getScore());
					log.info(a.toString());
					channel.basicPublish("", queue, null, mapper.writeValueAsBytes(a));
				}
				ctx.status(HttpStatus.HTTP_CREATED);
			}
		});
	}
}
