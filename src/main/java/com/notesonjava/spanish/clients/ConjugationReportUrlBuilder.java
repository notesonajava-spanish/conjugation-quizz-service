package com.notesonjava.spanish.clients;

import java.util.List;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ConjugationReportUrlBuilder {
	
	private String reportUrl;
	
	public String report(){
		return reportUrl + "/word";
	}

	public String report(List<String> verbs){
		StringBuilder sb = new StringBuilder();
		sb.append(reportUrl).append("/word");
		if(!verbs.isEmpty()) {
			sb.append("?");
			verbs.forEach(v -> sb.append("verb=").append(v).append("&"));
		}
		
		return sb.toString();
	}

	
	public String dailyReport(){
		return reportUrl + "/daily";
	}

	
	public String tiempoReport(){
		return reportUrl + "/tiempos";
	}

}
