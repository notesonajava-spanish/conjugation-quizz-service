package com.notesonjava.spanish.clients;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.notesonjava.spanish.model.Conjugation;
import com.notesonjava.spanish.model.Tiempo;
import com.notesonjava.spanish.model.WordStats;

import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ConjugationReportClient {

	private ConjugationReportUrlBuilder urlBuilder;

	private ObjectMapper mapper;
	
	public List<WordStats<Conjugation>> getReport(String token) throws JsonParseException, JsonMappingException, IOException{
		String url = urlBuilder.report();
		HttpResponse response = HttpRequest.get(url).tokenAuthentication(token).send();
		WordStats<Conjugation>[] stats = mapper.readValue(response.bodyBytes(), WordStats[].class);
		return Arrays.asList(stats);		
	}	
	
	public List<WordStats<LocalDate>> getLastWeekReport(String token) throws JsonParseException, JsonMappingException, IOException{
		String url = urlBuilder.dailyReport();
		HttpResponse response = HttpRequest.get(url).tokenAuthentication(token).send();
		WordStats<LocalDate>[] stats = mapper.readValue(response.bodyBytes(), WordStats[].class);
		return Arrays.asList(stats);	
	}
	
	public List<WordStats<Tiempo>> getTiempoReport(String token) throws JsonParseException, JsonMappingException, IOException{
		String url = urlBuilder.tiempoReport();
		HttpResponse response = HttpRequest.get(url).tokenAuthentication(token).send();
		WordStats<Tiempo>[] stats = mapper.readValue(response.bodyBytes(), WordStats[].class);
		return Arrays.asList(stats);	
	}
	
}
