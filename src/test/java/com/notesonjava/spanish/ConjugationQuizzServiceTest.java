package com.notesonjava.spanish;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.hamcrest.MockitoHamcrest;

import com.notesonjava.spanish.clients.ConjugationReportClient;
import com.notesonjava.spanish.clients.VerbClient;
import com.notesonjava.spanish.conjugation.ConjugationClient;
import com.notesonjava.spanish.conjugation.ConjugationQuizzService;
import com.notesonjava.spanish.dto.ConjugationResponse;
import com.notesonjava.spanish.model.Conjugation;
import com.notesonjava.spanish.model.Persona;
import com.notesonjava.spanish.model.Tiempo;
import com.notesonjava.spanish.model.VerbConjugation;

public class ConjugationQuizzServiceTest {
	
	private ConjugationReportClient reportClient;
	
	private ConjugationClient client;
	
	private VerbClient verbClient;
	
	private ConjugationQuizzService service;
	
	private static String USER = "user1";
	private static List<Conjugation> CONJ_LIST = new ArrayList<>();
	
	
	@BeforeClass
	public static void init(){
		for(int i=0; i<50; i++){
			Conjugation c = new Conjugation("word-"+i, Tiempo.IndicativoPresente, Persona.EL, "verb-"+i);
			CONJ_LIST.add(c);
		}		
	}
	
	@Before
	public void setup(){
		reportClient = Mockito.mock(ConjugationReportClient.class);
		client = Mockito.mock(ConjugationClient.class);
		verbClient = Mockito.mock(VerbClient.class);
		service = new ConjugationQuizzService(client, verbClient, reportClient);
	}
	
	
	@Test
	public void test_generate_conjugations() throws IOException {
		
		List<VerbConjugation> conjugationList = new ArrayList<>();
		for(int i=0; i<100; i++){
			VerbConjugation vc = new VerbConjugation();
			vc.setTiempo(Tiempo.Gerundio);
			vc.setVerb("v"+i);
			vc.addConjugation(new Conjugation("v-"+i+"o", Tiempo.Gerundio, Persona.NONE, "v"+i));
			conjugationList.add(vc);
		}
		
		
		Mockito.when(client.getConjugations(MockitoHamcrest.argThat(Matchers.any(List.class)), MockitoHamcrest.argThat(Matchers.any(Tiempo.class)))).thenReturn(conjugationList);
		
		ConjugationResponse response = new ConjugationResponse();
		response.setConjugations(CONJ_LIST);
		
		List<Conjugation> result = service.generate(USER, Arrays.asList("verb-1", "verb-2", "verb-3"), Tiempo.IndicativoPresente, 10);
		assertThat(result).hasSize(10);
		
	}	
}