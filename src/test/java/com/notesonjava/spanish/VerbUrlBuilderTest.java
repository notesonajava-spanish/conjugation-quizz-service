package com.notesonjava.spanish;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import com.notesonjava.spanish.clients.VerbUrlBuilder;
import com.notesonjava.spanish.model.Verb;
import com.notesonjava.spanish.model.WordStats;

public class VerbUrlBuilderTest {

    private VerbUrlBuilder builder = new VerbUrlBuilder("http://testurl");
    @Test
    public void popularVerbs() throws Exception {
        Assertions.assertThat(builder.popularVerbs()).isEqualTo("http://testurl/popular");
    }

    @Test
    public void popularVerbsWithLimit() throws Exception {
        Assertions.assertThat(builder.popularVerbs(200)).isEqualTo("http://testurl/popular?end=200");
    }

    @Test
    public void synonymsFromStats() throws Exception {
        Stream<String> words = Stream.of("hacer", "tener", "haber", "amar");

        List<WordStats<String>> list = words.map(w -> new WordStats<>(w)).collect(Collectors.toList());
        String popular = builder.synonymsFromStats(list);
        Assertions.assertThat(popular).isEqualTo("http://testurl/synonyms?verb=hacer&verb=tener&verb=haber&verb=amar");
    }

    @Test
    public void synonymsFromVerbs() throws Exception {

        Stream<String> words = Stream.of("hacer", "tener", "haber", "amar");

        List<Verb> list = words.map(w -> new Verb(w)).collect(Collectors.toList());
        String popular = builder.synonymsFromVerbs(list);
        Assertions.assertThat(popular).isEqualTo("http://testurl/synonyms?verb=hacer&verb=tener&verb=haber&verb=amar");
    }

    @Test
    public void suffixes() {
    	String url = builder.suffixes("enar", "anar");
        Assertions.assertThat(url).isEqualTo("http://testurl/suffix?suffix=enar&suffix=anar");

    }

}